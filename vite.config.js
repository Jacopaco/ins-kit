import { defineConfig } from 'vite';
import laravel from 'laravel-vite-plugin';
import vue2 from '@vitejs/plugin-vue2';

export default defineConfig({
    plugins: [
        laravel({
            input: [
                'resources/css/site.css',
                'resources/css/cp.css',
                'resources/js/site.js',
                'resources/js/cp.js',
                'resources/js/edit.js',
            ],
            refresh: true,
        }),
         vue2(),
    ],
});
