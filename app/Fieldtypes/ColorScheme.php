<?php

namespace App\Fieldtypes;

use Statamic\Fields\Fieldtype;
use Statamic\Facades\GlobalSet;

class ColorScheme extends Fieldtype
{
    /**
     * The blank/default value.
     *
     * @return array
     */
    public function defaultValue()
    {
        return null;
    }


    
    public function preload()
    {
 

        $colorScheme = [];
        $colors = GlobalSet::findByHandle('settings')->inDefaultSite()->get('color_scheme');

        foreach( $colors as $i => $color ){

            $colorScheme[]=  $color['color'];

        }

        return $colorScheme;

    }


    /**
     * Pre-process the data before it gets sent to the publish page.
     *
     * @param mixed $data
     * @return array|mixed
     */
    public function preProcess($data)
    {
        return $data;
    }

    /**
     * Process the data before it gets saved.
     *
     * @param mixed $data
     * @return array|mixed
     */
    public function process($data)
    {
        return $data;
    }
}
