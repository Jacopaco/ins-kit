<?php

namespace App\Tags;

use Statamic\Tags\Tags;
use Statamic\Facades\GlobalSet;

class Color extends Tags
{
    /**
     * The {{ color }} tag.
     *
     * @return string|array
     */
    public function index()
    {
        $settings = GlobalSet::findByHandle('settings');
    }

    /**
     * The {{ color:example }} tag.
     *
     * @return string|array
     */
    public function wildcard($utility)
    {

        $index = $this->params->get('index');

        switch ($utility) {

            case 'bg':
                return 'bg-c'.$index;
                break;
            case 'text':
                return 'text-c'.$index.'-text';
                break;
            case 'fill':
                return 'fill-c'.$index;
                break;

        }

    }

    public function raw()
    {

        //return hexcode

    }
}
