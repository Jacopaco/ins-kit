<?php

namespace App\Tags;

use Statamic\Tags\Tags;

class GetColor extends Tags
{
    /**
     * The {{ get_color }} tag.
     *
     * @return string|array
     */
    public function index()
    {
        //
    }

    /**
     * The {{ get_color:example }} tag.
     *
     * @return string|array
     */
    public function example()
    {
        //
    }
}
