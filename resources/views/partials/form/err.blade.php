<div class="" x-init="$refs.{{$field}}.setAttribute('valid','false')" >
    <ul>
        @if ( is_array($errors) )
            @foreach ($errors as $error)
                <li>{{ $error }}</li>
            @endforeach
        @else
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        @endif

    </ul>
</div>