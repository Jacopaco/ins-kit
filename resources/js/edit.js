
if( document.location.pathname.includes('entries') ){

    window.addEventListener('message', async (event) => {
    
        if (event.data.name === 'click') {

            //var groups = document.querySelectorAll('.groups-sortable-item');
            //var group = groups[event.data.group];

            var rows = document.querySelectorAll('.rows-sortable-item');
            //var rows = document.querySelectorAll('.groups\\['+ event.data.group +'\\]\\[rows\\]-sortable-item');
            var row = rows[event.data.row];
    
            var blocks = row.querySelectorAll('.rows\\['+ event.data.row +'\\]\\[blocks\\]-sortable-item');;
            var block = blocks[event.data.block];

           // if( group.querySelector('.replicator-set-header').classList.contains('collapsed') ){
           //     group.querySelector('.replicator-set-header-inner').click();
           // }
            if( row.querySelector('.replicator-set-header').classList.contains('collapsed') ){
                row.querySelector('.replicator-set-header-inner').click();
            }
            if( block.querySelector('.replicator-set-header').classList.contains('collapsed') ){
                block.querySelector('.replicator-set-header-inner').click();    
            }

            const outline = [
                { outline: "2px rgba(255, 0, 0, 0) solid" },
                { outline: "2px rgba(255, 0, 0, 1) solid" },
                { outline: "2px rgba(255, 0, 0, 0) solid" },
              ];
              
              const outlineTiming = {
                duration: 1500,
                iterations: 1,
              };
              

            setTimeout(() => {
                block.scrollIntoView({ behavior: "smooth" });
                block.querySelector('.replicator-set').animate(outline, outlineTiming)
              }, 2);
    
        }
    });    

}

Statamic.$conditions.add('1_blocks', ({ target }) => {
    return target.length < 2;
});
Statamic.$conditions.add('2_blocks', ({ target }) => {
    return target.length == 2;
});
Statamic.$conditions.add('3_blocks', ({ target }) => {
    return target.length == 3;
});
Statamic.$conditions.add('4_blocks', ({ target }) => {
    return target.length == 4;
});