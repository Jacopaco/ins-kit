/**
 * When extending the control panel, be sure to uncomment the necessary code for your build process:
 * https://statamic.dev/extending/control-panel
 */

 //Example Fieldtype
// This is all you.


import ColorScheme from './components/fieldtypes/ColorScheme.vue';
 
// Should be named [snake_case_handle]-fieldtype

Statamic.$components.register('color_scheme-fieldtype', ColorScheme);