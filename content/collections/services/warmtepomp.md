---
id: c540a18a-f35a-43df-b1ce-9148158a97e8
blueprint: service
title: Warmtepomp
updated_by: 8b47395d-a977-43a9-b00a-1a39eb5fd6a4
updated_at: 1702494952
image: 1oigsjhlmv4kvtrcbjddxetuc3vnerbo6mhpgmtr.jpg
icon: leaf
rows:
  -
    id: lq459z67
    blocks:
      -
        id: lq45a0h2
        link_block: false
        block_content:
          -
            type: heading
            attrs:
              textAlign: left
              level: 1
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: Warmtepompen
          -
            type: paragraph
            attrs:
              textAlign: left
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Wilt u graag besparen op uw maandelijkse energiekosten, zonder dat u hoeft in te leveren op comfort? Dan is een warmtepomp een uitstekende oplossing! M. v.d. Vecht Installatietechniekkan u alles vertellen over de verschillende soorten warmtepompen. Daarbij kunnen wij u adviseren over met welke warmtepomp u van het hoogste rendement kunt profiteren.'
        scroll_animate: none
        rotation: 0
        frame: false
        frame_type: blob-1
        type: block
        enabled: true
      -
        id: lq45adzl
        link_block: false
        block_content:
          -
            type: set
            attrs:
              id: lq45ahm7
              values:
                type: image
                absolute: true
                image: img/1oigsjhlmv4kvtrcbjddxetuc3vnerbo6mhpgmtr.jpg
        scroll_animate: none
        rotation: 0
        frame: false
        frame_type: blob-1
        type: block
        enabled: true
    type: row
    enabled: true
    distribution_2: 1/2|1/2
    blocks_align: center
    background_color: 1
  -
    id: lq45bz7l
    position: -67
    type: divider
    enabled: true
  -
    id: lq45axoy
    blocks:
      -
        id: lq45azag
        link_block: false
        block_content:
          -
            type: heading
            attrs:
              textAlign: left
              level: 2
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Lucht-water warmtepomp'
          -
            type: paragraph
            attrs:
              textAlign: left
            content:
              -
                type: text
                text: 'Een lucht-water warmtepomp onttrekt warmte aan de buitenlucht en warmt deze verder op, waarna de warmte aan het water wordt afgegeven. Het verwarmde water wordt dan gebruikt om de woning te verwarmen en/of van warm water te voorzien. Hoewel het rendement van een lucht-water warmtepomp gedurende de koudere maanden van het jaar wel wat lager ligt, kunt u het hele jaar door van een mooie besparing op uw energierekening profiteren. De installatie vereist de plaatsing van een binnen- en buitenunit, en is ook voor bestaande woningen een goede oplossing om energie te besparen. Daarbij zijn sommige lucht-water warmtepompen ook in staat om te koelen.'
          -
            type: heading
            attrs:
              textAlign: left
              level: 2
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Ventilatielucht-water warmtepomp'
          -
            type: paragraph
            attrs:
              textAlign: left
            content:
              -
                type: text
                text: 'Een ventilatielucht-water warmtepomp wordt gecombineerd met een mechanisch ventilatiesysteem. De warmte wordt aan de ventilatielucht onttrokken en door de warmtepomp omgezet om de woning te verwarmen en warm water te leveren. Een groot voordeel van de ventilatie-lucht warmtepomp is dat er geen ingrijpende werkzaamheden nodig zijn en dat er ook geen buitenunit geplaatst hoeft te worden. Een mechanische ventilatiebox of WTW-systeem kan doorgaans prima door de warmtepomp vervangen worden. Dit type warmtepomp is vooral geschikt voor goed-geïsoleerde woningen die relatief weinig energie verbruiken.'
          -
            type: heading
            attrs:
              textAlign: left
              level: 2
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Water-water warmtepomp'
          -
            type: paragraph
            attrs:
              textAlign: left
            content:
              -
                type: text
                text: 'Een water-water warmtepomp onttrekt de warmte aan het grondwater en gebruikt dit vervolgens om de woning te verwarmen. Vanwege de werkzaamheden die vereist zijn voor de installatie van dit type warmtepomp (er moeten putten worden gegraven) worden ze vrijwel alleen bij nieuwbouwwoningen toegepast. Een water-water warmtepomp vereist een grotere investering dan andere typen warmtepompen, maar levert ook het hoogste rendement op.'
          -
            type: heading
            attrs:
              textAlign: left
              level: 2
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: Subsidie
          -
            type: paragraph
            attrs:
              textAlign: left
            content:
              -
                type: text
                text: 'De overheid wil mensen mensen stimuleren om in duurzame oplossingen te investeren. Dit doen zij door subsidies te verstrekken wanneer u energiebesparende maatregelen treft. U kunt mogelijk dus aanspraak maken op een tegemoetkoming als u een warmtepomp laat plaatsen. Wij kunnen u alles vertellen over de voorwaarden die op de subsidies van toepassing zijn en u begeleiden bij de subsidieaanvraag.'
        scroll_animate: none
        rotation: 0
        frame: false
        frame_type: blob-1
        type: block
        enabled: true
    distribution_1: 1/1
    background_color: 0
    type: row
    enabled: true
  -
    id: lq45fjmz
    position: 68
    type: divider
    enabled: true
  -
    id: lq45e1xm
    blocks:
      -
        id: lq45e343
        link_block: false
        block_content:
          -
            type: heading
            attrs:
              textAlign: left
              level: 2
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Ja, ik heb interesse in warmtepompen!'
          -
            type: paragraph
            attrs:
              textAlign: left
            content:
              -
                type: text
                text: 'Wilt u graag meer weten over de mogelijkheden van een warmtepomp? Neem dan contact op met M. v.d. Vecht Installatietechniek. We vertellen u graag meer over de warmtepomp waarmee u van het hoogste rendement kunt profiteren. Ook kunnen wij de investering, terugverdientijd en besparingsmogelijkheden voor u in kaart brengen.'
        scroll_animate: none
        rotation: 0
        frame: false
        frame_type: blob-1
        type: block
        enabled: true
      -
        id: lq45e5ti
        link_block: false
        block_content:
          -
            type: set
            attrs:
              id: lq45ek0i
              values:
                type: form
                form: contact
        scroll_animate: none
        rotation: 0
        frame: true
        frame_type: blob-1
        type: block
        enabled: true
    distribution_2: 1/2|1/2
    blocks_align: end
    background_color: 1
    type: row
    enabled: true
  -
    id: lq45hyfp
    _row: 2631b5e9-8df5-424e-bec6-5174084348d9
    type: _row
    enabled: true
---
