---
id: 4a8c21d8-e20f-4f48-bb09-33dd61d501a0
blueprint: project
title: 'Nieuwbouw Woonboerderij'
media:
  - vybwpo1q1ozdroypyqw7r8r8mz38ydnmtjq6dpeo.jpg
  - aoaymqb334qu5dtlsl5tq2ht9sogpqp6nxsawhgw.jpg
  - 8xzyl5qitjjvitx89rcirirl1czbskpraxrudkuk.jpg
  - zlkrvkggvikacusbag9vlqhptqrrjdignvjgr8tb.jpg
location:
  street: Waalbandijk
  city: Neerijnen
  country: NL
  latitude: '51.8308432'
  longitude: '5.2834911'
ma_zoom: 8
author: 8b47395d-a977-43a9-b00a-1a39eb5fd6a4
updated_by: 8b47395d-a977-43a9-b00a-1a39eb5fd6a4
updated_at: 1702455910
services:
  - c540a18a-f35a-43df-b1ce-9148158a97e8
  - c56fd34f-80d4-42e7-a74a-f9e503224b56
  - a30f2e29-ec01-4a22-b66a-d5b6b1d274b2
---
Nieuwbouw Woonboerderij Neerijnen Water-waterwarmtepomp
Wilt u meer informatie over wat een waterwatermtepomp voor u kan betekenen? Lees dan onze pagina over warmtepompen.