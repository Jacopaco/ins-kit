---
id: ba2c52c6-45ab-4d60-b9b7-8bd98a7f57f2
blueprint: project
title: 'Voormalige rechtbank'
author: 8b47395d-a977-43a9-b00a-1a39eb5fd6a4
location:
  name: null
  street: 'Nieuwe tielse weg'
  street2: null
  postCode: null
  city: Tiel
  country: NL
  latitude: 51.882213345341
  longitude: 5.4256582260132
updated_by: 8b47395d-a977-43a9-b00a-1a39eb5fd6a4
updated_at: 1702545994
map:
  lng: 5.586512318065
  lat: 52.137453758688
  markerLng: 5.5862451967432
  markerLat: 52.13732528955
  zoom: 17
  type: streets-v11
ma_zoom: 14
media:
  - lprrnklsns5vozbvdvk9byugbqm96nt0kekeoqs7.jpg
  - d8gpmexffomt5kue1vlrv8piaxaoqlwd69r9yqea.jpg
  - epdlfbwpr5xkovlfs7gbmqvgd1xsiw3bb2onrkgt.jpg
  - ozye9awvqtx7k9boaay7l0ixglymq6fsggb6ztrp.jpg
  - pruy647833wfveooywrjxzb8bzb5tv3aatqvwtex.jpg
  - uiericklizv7hqbkyvplg7xj1pz4xyxqyps9chob.jpg
services:
  - 80874a27-a8e8-405f-9e72-6d4635631951
  - 7b07bb65-4162-4325-8f18-7d7ca09f6010
  - a30f2e29-ec01-4a22-b66a-d5b6b1d274b2
  - 8c168cfa-b8d3-4332-a3c4-42351ea53888
  - c540a18a-f35a-43df-b1ce-9148158a97e8
Services_list:
  - test
---
Voor de voormalige rechtbank Tiel hebben wij de volledige installatie verzorgd.