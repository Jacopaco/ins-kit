---
id: 1b5609e7-1292-4e94-8c43-7e716326ed66
blueprint: project
title: 'Rijksen Speciaal Timmerwerk'
media:
  - xwb8xasexgghjehzjzn9sizt8owuuhplk02qufia.jpg
  - xlnsqqj5kwrgqeyfkewjsdjauv3ic1fwnc2gvc9d.jpg
location:
  street: Doejenburg
  city: Maurik
  country: NL
  latitude: '51.965643650000004'
  longitude: '5.440655585322816'
ma_zoom: 8
author: 8b47395d-a977-43a9-b00a-1a39eb5fd6a4
updated_by: 8b47395d-a977-43a9-b00a-1a39eb5fd6a4
updated_at: 1701926969
---
Voor dit nieuwe bedrijfspand op de Doejenburg 2 in Maurik hebben wij de complete installatie aangelegd.