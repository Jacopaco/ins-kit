---
id: 2631b5e9-8df5-424e-bec6-5174084348d9
blueprint: component
title: Footer
blocks:
  -
    id: lq3w6q1q
    link_block: false
    block_content:
      -
        type: heading
        attrs:
          textAlign: left
          level: 3
        content:
          -
            type: text
            text: '{{  bedrijf:firmanaam }}'
      -
        type: set
        attrs:
          id: lq3w7mbq
          values:
            type: marketing_list
            marketing_list:
              -
                id: lq3w7nhg
                icon: map-location-dot
                text: '{{ bedrijf:straat }} {{ bedrijf:huisnummer }}, {{ bedrijf:postcode }} {{ bedrijf:plaats }}'
              -
                id: lq3w7ony
                icon: phone
                text: '{{ bedrijf:telefoon }}'
              -
                id: lq3wduze
                icon: paper-plane
                text: '{{ bedrijf:email }}'
    scroll_animate: none
    rotation: 0
    frame: false
    frame_type: blob-1
    type: block
    enabled: true
  -
    id: lq3wifqo
    link_block: false
    scroll_animate: none
    rotation: 0
    frame: false
    frame_type: blob-1
    type: block
    enabled: true
    block_content:
      -
        type: heading
        attrs:
          textAlign: left
          level: 3
        content:
          -
            type: text
            marks:
              -
                type: bold
            text: 'Erkend installateur:'
      -
        type: bulletList
        content:
          -
            type: listItem
            content:
              -
                type: paragraph
                attrs:
                  textAlign: left
                content:
                  -
                    type: text
                    text: 'Waterleidingtechniek CE 361'
          -
            type: listItem
            content:
              -
                type: paragraph
                attrs:
                  textAlign: left
                content:
                  -
                    type: text
                    text: 'Gastechniek CE 360'
          -
            type: listItem
            content:
              -
                type: paragraph
                attrs:
                  textAlign: left
                content:
                  -
                    type: text
                    text: 'EVI Gastechnische inst.'
          -
            type: listItem
            content:
              -
                type: paragraph
                attrs:
                  textAlign: left
                content:
                  -
                    type: text
                    text: 'EVI Waterpompinstallaties'
          -
            type: listItem
            content:
              -
                type: paragraph
                attrs:
                  textAlign: left
                content:
                  -
                    type: text
                    text: 'EVI Watertechn. inst.'
  -
    id: lq3wl3k9
    link_block: false
    block_content:
      -
        type: heading
        attrs:
          textAlign: left
          level: 3
        content:
          -
            type: text
            marks:
              -
                type: bold
            text: 'Social media:'
      -
        type: set
        attrs:
          id: lq3wlipr
          values:
            type: marketing_list
            marketing_list:
              -
                id: lq3wljee
                icon: facebook-f
                text: Facebook
              -
                id: lq3wljw6
                icon: linkedin-in
                text: LinkedIn
              -
                id: lq3wlkcz
                icon: instagram
                text: Instagram
    scroll_animate: none
    rotation: 0
    frame: false
    frame_type: blob-1
    type: block
    enabled: true
background_color: 1
updated_by: 8b47395d-a977-43a9-b00a-1a39eb5fd6a4
updated_at: 1702500384
blocks_align: start
distribution_3: 1/2|1/4|1/4
extra_padding: true
---
