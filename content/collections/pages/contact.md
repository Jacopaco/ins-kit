---
id: 9135feb1-dcb0-451f-8c00-d51933867b93
blueprint: page
title: Contact
author: 8b47395d-a977-43a9-b00a-1a39eb5fd6a4
updated_by: 8b47395d-a977-43a9-b00a-1a39eb5fd6a4
updated_at: 1702499121
rows:
  -
    id: lq477trw
    blocks:
      -
        id: lq477y8l
        link_block: false
        block_content:
          -
            type: heading
            attrs:
              textAlign: left
              level: 1
            content:
              -
                type: text
                text: Contactgegevens
          -
            type: set
            attrs:
              id: lq47ay14
              values:
                type: marketing_list
                marketing_list:
                  -
                    id: lq47ayla
                    icon: map-location-dot
                    text: '{{ bedrijf:straat }} {{ bedrijf:huisnummer }}, {{ bedrijf:postcode }} {{ bedrijf:plaats }}'
                  -
                    id: lq47azlk
                    icon: phone-flip
                    text: '{{ bedrijf:telefoon }}'
                  -
                    id: lq47b0xz
                    icon: paper-plane
                    text: '{{ bedrijf:email }}'
        scroll_animate: none
        rotation: 0
        frame: false
        frame_type: blob-1
        type: block
        enabled: true
      -
        id: lq4789nb
        link_block: false
        block_content:
          -
            type: set
            attrs:
              id: lq47hocw
              values:
                type: image
                absolute: false
                image: img/x4vfwrkz2v55fhniqkjbiodxjxh1vft2qbvwvia8-1702499118.jpg
        scroll_animate: none
        rotation: 0
        frame: false
        frame_type: blob-1
        type: block
        enabled: true
    distribution_2: 1/2|1/2
    background_color: 1
    type: row
    enabled: true
  -
    id: lq47idi8
    position: 144
    type: divider
    enabled: true
  -
    id: lq47guet
    blocks:
      -
        id: lq47gxd4
        link_block: false
        block_content:
          -
            type: set
            attrs:
              id: lq47h0z5
              values:
                type: form
                form: contact
        scroll_animate: none
        rotation: 0
        frame: true
        frame_type: blob-1
        type: block
        enabled: true
    distribution_1: 1/1
    background_color: 0
    type: row
    enabled: true
---
