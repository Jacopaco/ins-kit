---
id: home
blueprint: pages
title: Home
template: default
rows:
  -
    id: lpqxozoo
    blocks:
      -
        id: lpqxp3ev
        scroll_animate: slide
        rotation: 0
        frame: true
        frame_type: blob-1
        type: block
        enabled: true
        block_content:
          -
            type: heading
            attrs:
              textAlign: left
              level: 1
            content:
              -
                type: text
                text: 'Betrouwbare installateur nodig?'
          -
            type: paragraph
            attrs:
              textAlign: left
            content:
              -
                type: text
                text: 'M. v.d. Vecht Installatietechniek is een gerenommeerd en veelzijdig installatiebedrijf. '
          -
            type: paragraph
            attrs:
              textAlign: left
            content:
              -
                type: text
                text: 'Onze betrokken medewerkers zijn de drijvende kracht achter het bedrijf. Ons team heeft een klantgerichte instelling. Daarbij ontzorgen wij de klant door het leveren van goede kwaliteit, service en nazorg.'
        link_block: false
      -
        id: lpr1av91
        link_block: false
        block_content:
          -
            type: set
            attrs:
              id: lq3rojwo
              values:
                type: marketing_list
                marketing_list:
                  -
                    id: lq3roku0
                    icon: star
                    text: 'Oog voor kwaliteit'
                  -
                    id: lq3y2qep
                    icon: helmet-safety
                    text: 'Ervaren monteurs'
                  -
                    id: lq45or9g
                    icon: people-group
                    text: 'Persoonlijk contact'
                  -
                    id: lq45p4s1
                    icon: leaf
                    text: 'Duurzaamheid staat voorop'
        scroll_animate: slide
        rotation: 0
        frame: true
        frame_type: blob-1
        type: block
        enabled: true
    type: row
    enabled: true
    distribution_2: 2/3|1/3
    background_color: 1
    blocks_align: start
    background_media: vybwpo1q1ozdroypyqw7r8r8mz38ydnmtjq6dpeo.jpg
    extra_padding: false
  -
    id: lpr94bo8
    type: divider
    enabled: true
    position: -79
  -
    id: lpqxp12m
    blocks:
      -
        id: lpqxp8zn
        scroll_animate: none
        rotation: 0
        frame: false
        frame_type: blob-1
        type: block
        enabled: true
        block_content:
          -
            type: set
            attrs:
              id: lptq6y9y
              values:
                type: collection_tiles
                collection: services
                selection: true
                amount: 1
                items:
                  - 7b07bb65-4162-4325-8f18-7d7ca09f6010
                  - a30f2e29-ec01-4a22-b66a-d5b6b1d274b2
                  - c56fd34f-80d4-42e7-a74a-f9e503224b56
                  - c540a18a-f35a-43df-b1ce-9148158a97e8
                  - 8c168cfa-b8d3-4332-a3c4-42351ea53888
                  - 80874a27-a8e8-405f-9e72-6d4635631951
                collection_type: services
        link_block: false
    type: row
    enabled: true
    background_color: 1
    row_heading: 'Waar kunnen wij mee helpen?'
    distribution_1: 1/1
    extra_padding: false
  -
    id: lpu2rlrt
    position: -68
    type: divider
    enabled: true
  -
    id: lptrfb5m
    row_heading: 'Recente projecten'
    blocks:
      -
        id: lptrfc79
        link_block: false
        block_content:
          -
            type: set
            attrs:
              id: lptrfg0i
              values:
                type: collection_tiles
                collection: projects
                amount: 1
                selection: false
                collection_type: projects
        scroll_animate: none
        rotation: 0
        frame: false
        frame_type: blob-1
        type: block
        enabled: true
    distribution_1: 1/1
    background_color: 0
    type: row
    enabled: true
    extra_padding: false
  -
    id: lptycrg0
    blocks:
      -
        id: lptyfdnp
        link_block: false
        scroll_animate: none
        rotation: 0
        frame: true
        frame_type: blob-1
        type: block
        enabled: true
        block_content:
          -
            type: heading
            attrs:
              textAlign: left
              level: 3
            content:
              -
                type: text
                text: 'Bel {{ bedrijf:telefoon  }}'
          -
            type: paragraph
            attrs:
              textAlign: left
            content:
              -
                type: text
                text: 'of vul het formulier in.'
          -
            type: set
            attrs:
              id: lptymiia
              values:
                type: form
                form: contact
      -
        id: lptyct1p
        link_block: false
        block_content:
          -
            type: set
            attrs:
              id: lq3kqqv7
              values:
                type: marketing_list
                marketing_list:
                  -
                    id: lq3kqswe
                    icon: star
                    text: 'Oog voor kwaliteit'
                  -
                    id: lq3ksy0x
                    icon: helmet-safety
                    text: 'Ervaren monteurs'
                  -
                    id: lq3m0jwe
                    icon: handshake-angle
                    text: 'Persoonlijk contact'
                  -
                    id: lq3m0vzh
                    icon: leaf
                    text: 'Duurzaamheid staat voorop'
                  -
                    id: lq3m1e7b
                    icon: wrench
                    text: 'Gewoon goede service'
                  -
                    id: lq3m1yhz
                    icon: map-location-dot
                    text: 'Installateur uit uw regio'
        scroll_animate: none
        rotation: 0
        frame: true
        frame_type: blob-1
        type: block
        enabled: true
    distribution_2: 1/2|1/2
    background_color: 0
    type: row
    enabled: true
    blocks_align: start
    row_heading: 'Gaan we samen iets moois maken?'
    extra_padding: false
  -
    id: lq3wzqxv
    position: 47
    type: divider
    enabled: true
  -
    id: lq3x0er3
    distribution_1: 1/1
    background_color: 1
    type: row
    enabled: true
    extra_padding: false
  -
    id: lq3vepln
    _row: 2631b5e9-8df5-424e-bec6-5174084348d9
    type: _row
    enabled: true
author: 8b47395d-a977-43a9-b00a-1a39eb5fd6a4
updated_by: 546372b3-031a-4fa1-9cc7-cd006d38982e
updated_at: 1703950515
---
## Welcome to your brand new Statamic site!

Not sure where to do next? Here are a few ideas, but feel free to explore in your own way, in your own time.

- [Jump into the Control Panel](/cp) and edit this page or begin setting up your own collections and blueprints.
- [Head to the docs](https://statamic.dev) and learn how Statamic works.
- [Watch some Statamic videos](https://youtube.com/statamic) on YouTube.
- [Join our Discord chat](https://statamic.com/discord) and meet thousands of other Statamic developers.
- [Start a discussion](https://github.com/statamic/cms/discussions) and get answers to your questions.
- [Star Statamic on Github](https://github.com/statamic/cms) if you enjoy using it!
