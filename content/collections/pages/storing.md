---
id: 92ac9445-9860-4204-a903-dfedbd0a46a1
blueprint: page
title: Storing
author: 8b47395d-a977-43a9-b00a-1a39eb5fd6a4
updated_by: 8b47395d-a977-43a9-b00a-1a39eb5fd6a4
updated_at: 1702500302
rows:
  -
    id: lq3xpub1
    blocks:
      -
        id: lq3xq6z4
        link_block: false
        block_content:
          -
            type: heading
            attrs:
              textAlign: left
              level: 3
            content:
              -
                type: text
                text: 'Voor als je snel zelf aan de slag wil.'
          -
            type: set
            attrs:
              id: lq3xspwm
              values:
                type: accordion
                accordion:
                  -
                    id: lq3xsqv9
                    heading: 'Storing CV-ketel: wat kunt u zelf doen?'
                    description:
                      -
                        type: paragraph
                        content:
                          -
                            type: text
                            text: '1. Controleer of alle radiatorknoppen open staan. Zet zonodig alle radiatorkranen in de huiskamer open. Indien alleen de radiatoren boven koud blijven, dient u mogelijk water bij te vullen; het is sowieso raadzaam allereerst de radiatoren te ontluchten. Indien alle radiatoren koud blijven, gaat u naar stap 2.'
                      -
                        type: paragraph
                        content:
                          -
                            type: text
                            text: '2. Indien u een combiketel heeft: controleer of alle warm waterkranen dicht zijn. Draai ze zonodig dicht. Zijn alle kranen dicht en het probleem blijft, ga dan naar stap 3.'
                      -
                        type: paragraph
                        content:
                          -
                            type: text
                            text: '3. Controleer de waterdruk van de verwarmingsinstallatie. Deze dient tussen de 1,2 en 2 bar te zijn. Is deze lager dan dient u water bij te vullen. Is deze niet lager gaat u naar stap 4.'
                      -
                        type: paragraph
                        content:
                          -
                            type: text
                            text: '4. Controleer of er stroom op het stopcontact bij de ketel staat. Is er geen stroom? Dan dient u uw elektricien in te schakelen. Als er wel stroom is, gaat u door naar stap 5.'
                      -
                        type: paragraph
                        content:
                          -
                            type: text
                            text: '5. Controleer of het display op de kamerthermostaat zichtbaar is. Indien dit niet het geval is, dienen wellicht de batterijen vervangen te worden. Raadpleeg de handleiding van uw kamerthermostaat hoe u dit kunt doen. Heeft u wel een display, ga dan door naar stap 6.'
                      -
                        type: paragraph
                        content:
                          -
                            type: text
                            text: "6. Indien de ketel is voorzien van een resetknop, dient u deze in te drukken nadat u de eventuele storingscode heeft genoteerd.\_"
                      -
                        type: paragraph
                        content:
                          -
                            type: text
                            text: '7. U kunt de storing niet zelf verhelpen. Noteer dan de storingscode, het fabrikaat, type en bouwjaar van het toestel. Neem vervolgens contact met ons op.'
                  -
                    id: lq3xt18y
                    heading: 'Geen warm water: wat kunt u zelf doen?'
                    description:
                      -
                        type: paragraph
                        content:
                          -
                            type: hardBreak
                          -
                            type: text
                            text: '1. Indien u een combiketel heeft: controleer of de verwarming het wel doet. Geef dit door bij het eventueel melden van de storing. Ga naar stap 2.'
                      -
                        type: paragraph
                        content:
                          -
                            type: text
                            text: '2. Indien u een combiketel heeft: controleer of alle warm waterkranen dicht zijn. Draai ze zonodig dicht. Zijn alle kranen dicht en het probleem blijft, ga dan naar stap 3.'
                      -
                        type: paragraph
                        content:
                          -
                            type: text
                            text: "3. Controleer op elke kraan in uw huis of er warm water komt. Indien er bijvoorbeeld overal warm water komt, behalve in de douche, dan kan het probleem niet aan het gastoestel liggen. U dient de kraan of de douchekop, waaruit geen warm water komt, goed te ontkalken.\_"
                      -
                        type: paragraph
                        content:
                          -
                            type: text
                            text: "4. Komt er nog steeds uit geen enkele waterkraan warm water, bel dan\_0344 606 292."
                      -
                        type: paragraph
                  -
                    id: lq3xtbq7
                    heading: 'Hoe krijg ik een warmtepomp storing weg ?'
                    description:
                      -
                        type: paragraph
                        content:
                          -
                            type: text
                            text: 'Begint u altijd met het kort noteren van de storing informatie: Bijvoorbeeld: 28-9-2016 de LED is rood en er knippert een cijfer ‘2’. Deze informatie is later belangrijk voor de installateur (natuurlijk zijn er ook al warmtepompen die deze geschiedenis zelf opslaan in een zgn. log file, maar toch noteert u de storing, ook een log file kan verloren gaan.'
                          -
                            type: hardBreak
                          -
                            type: text
                            text: 'Bij een storing kunt u vaak proberen het toestel een ‘reset’ te geven. Doe dit maar één keer binnen 24 uur. Mogelijk is de storing dan al verholpen. Komt het hierna toch weer voor, bel dan de installateur. Hoe ‘reset’ u het toestel ? Sommige warmtepompen hebben een duidelijke ‘reset’ knop, andere warmtepompen een makkelijk menu wat bij storing meteen de optie ‘reset’ biedt. Helaas zijn er ook warmtepompen (vooral de oudere die nog niet de laatste techniek hebben) waarbij het resetten min of meer verstopt zit in een onduidelijke menu structuur / bediening. Is er een duidelijk ‘reset’ mogelijkheid gebruik die dan! Wat kan de consument / gebruiker zelf nog controleren of doen ?'
                          -
                            type: hardBreak
                          -
                            type: text
                            text: 'Naast de eenmalige reset binnen 24 uur (dus nooit 2x achter elkaar!) kunt u natuurlijk controleren of er voldoende water in het cv-systeem zit en of er voldoende water/glycol in het bron systeem zit;'
                      -
                        type: paragraph
                        content:
                          -
                            type: hardBreak
                          -
                            type: text
                            text: 'De manometer in de cv leiding moet tussen de 0,9 en 2 bar staan, de manometer in het bron systeem moet tussen de 0,5 en 2 bar staan. In plaats van een manometer hebben sommige installaties een doorzichtig drukvat in de bron installatie zitten, dit vat moet minimaal voor 30% zijn gevuld met water/glycol, bijvullen gebeurt tot 75%. Indien de druk aan de cv zijde / afgifte zijde lager is kunt u deze bijvullen met water (net als bij een cv-ketel installatie) indien de bron zijde ’n klein beetje te weinig water/glycol heeft kunt u deze bijvullen met water, u dient dan echter wel de installateur te vragen om op korte termijn het glycolgehalte te controleren.'
        scroll_animate: none
        rotation: 0
        frame: false
        frame_type: blob-1
        type: block
        enabled: true
      -
        id: lq3xqnyu
        link_block: false
        block_content:
          -
            type: heading
            attrs:
              textAlign: left
              level: 3
            content:
              -
                type: text
                text: 'Kom je er niet uit? '
          -
            type: heading
            attrs:
              textAlign: left
              level: 3
            content:
              -
                type: text
                text: 'Bel {{ bedrijf:telefoon }}'
          -
            type: paragraph
            attrs:
              textAlign: left
            content:
              -
                type: text
                text: 'Of vul het formulier in.'
          -
            type: set
            attrs:
              id: lq3xred1
              values:
                type: form
                form: storing
        scroll_animate: none
        rotation: 0
        frame: true
        frame_type: blob-1
        type: block
        enabled: true
    distribution_2: 2/3|1/3
    type: row
    enabled: true
    row_heading: 'Storing melden'
    background_color: 0
    extra_padding: true
  -
    id: lq3xwdz9
    _row: 2631b5e9-8df5-424e-bec6-5174084348d9
    type: _row
    enabled: true
---
