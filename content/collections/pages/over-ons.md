---
id: e64ade6b-f0f3-42bc-8d6e-081e6d9f5f29
blueprint: page
title: 'Over ons'
author: 8b47395d-a977-43a9-b00a-1a39eb5fd6a4
updated_by: 8b47395d-a977-43a9-b00a-1a39eb5fd6a4
updated_at: 1702500355
rows:
  -
    id: lq3xjkil
    blocks:
      -
        id: lq3xjrux
        link_block: false
        block_content:
          -
            type: heading
            attrs:
              textAlign: left
              level: 1
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Over ons'
          -
            type: paragraph
            attrs:
              textAlign: left
            content:
              -
                type: text
                text: 'M v.d. Vecht is een installatiebedrijf uit Eck en Wiel. We zijn specialist in alle aspecten van de installatietechniek. In de loop der jaren hebben wij een grote klantenkring van particulieren, kleine en middelgrote bedrijven opgebouwd.'
          -
            type: paragraph
            attrs:
              textAlign: left
            content:
              -
                type: text
                text: "M v.d. Vecht is een totaalinstallateur die u een complete oplossing kan bieden, van ontwerp tot service en onderhoud. Ook denken wij aan het milieu want duurzaamheid staat voor ons voorop. Wij staan dicht bij de klant, we denken mee, geven advies en leveren kwaliteit. Wat erg belangrijk voor ons is dat we samen met de klant tevreden op het project terug kunnen kijken en we met een goed gevoel op weg naar de volgende project kunnen gaan.\_"
          -
            type: paragraph
            attrs:
              textAlign: left
            content:
              -
                type: text
                text: 'Heeft u vragen of wilt u een offerte aanvragen? Neem dat gerust contact met ons op.'
        scroll_animate: none
        rotation: 0
        frame: false
        frame_type: blob-1
        type: block
        enabled: true
      -
        id: lq3xjwlr
        link_block: false
        block_content:
          -
            type: set
            attrs:
              id: lq3xk0h9
              values:
                type: image
                absolute: true
                image: img/1oigsjhlmv4kvtrcbjddxetuc3vnerbo6mhpgmtr.jpg
        scroll_animate: none
        rotation: 0
        frame: false
        frame_type: blob-1
        type: block
        enabled: true
    distribution_2: 1/2|1/2
    type: row
    enabled: true
    background_color: 0
    extra_padding: true
  -
    id: lq3xj3h3
    _row: 2631b5e9-8df5-424e-bec6-5174084348d9
    type: _row
    enabled: true
---
