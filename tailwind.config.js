/** @type {import('tailwindcss').Config} */

const colors = require('./colors');

export default {
  content: [
    './resources/**/*.antlers.html',
    './resources/**/*.antlers.php',
    './resources/**/*.blade.php',
    './resources/**/*.vue',
    './content/**/*.md'
  ],
  theme: {
    fontFamily: {
      'display': ['Chunk'],
      'body': ['LeagueSpartan'],
      'notoEmoji': ['notoEmoji']
    },
    extend: {
      colors: colors, 
      width: {
        'c1': 'calc(1/12*100% - (1 - 1/12)*var(--g-gutter))',
        'c1-ng': 'calc(1/12*100%)',
        'c2': 'calc(2/12*100% - (1 - 2/12)*var(--g-gutter))',
        'c2-ng': 'calc(2/12*100%)', 
        'c3': 'calc(3/12*100% - (1 - 3/12)*var(--g-gutter))',
        'c3-ng': 'calc(3/12*100%)',
        'c4': 'calc(4/12*100% - (1 - 4/12)*var(--g-gutter))',
        'c4-ng': 'calc(4/12*100%)',
        'c5': 'calc(5/12*100% - (1 - 5/12)*var(--g-gutter))',
        'c5-ng': 'calc(5/12*100%)',
        'c6': 'calc(6/12*100% - (1 - 6/12)*var(--g-gutter))',
        'c6-ng': 'calc(6/12*100%)',
        'c7': 'calc(7/12*100% - (1 - 7/12)*var(--g-gutter))',
        'c7-ng': 'calc(7/12*100%)',
        'c8': 'calc(8/12*100% - (1 - 8/12)*var(--g-gutter))',
        'c8-ng': 'calc(8/12*100%)',
        'c9': 'calc(9/12*100% - (1 - 9/12)*var(--g-gutter))',
        'c9-ng': 'calc(9/12*100%)',
        'c10': 'calc(10/12*100% - (1 - 10/12)*var(--g-gutter))',
        'c10-ng': 'calc(10/12*100%)',
        'c11': 'calc(11/12*100% - (1 - 11/12)*var(--g-gutter))',
        'c11-ng': 'calc(10/12*100%)',
      },
      gap: {
        'theme': 'var(--g-gutter)',
      },
      padding: {
        'theme': 'var(--g-padding)',
      }
    },
  },
  plugins: [
    require('@tailwindcss/typography'),
    require('tailwind-scrollbar-hide')
  ],
}

