module.exports = {
    c0: 'var(--colors_0)',
    c1: 'var(--colors_1)',
    c2: 'var(--colors_2)',
    'c0-text': 'var(--colors_0_text)',
    'c1-text': 'var(--colors_1_text)',
    'c2-text': 'var(--colors_2_text)',
    links: 'var(--colors_links)',
}